/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.program_ox;

/**
 *
 * @author Pee
 */
import java.util.Scanner;

public class OXGame {
    private char[][] board;
    private char currentPlayer;

    public OXGame() {
        board = new char[3][3];
        currentPlayer = 'X';
        initializeBoard();
    }

    public void playGame() {
        boolean gameEnd = false;
        int row, col;

        while (!gameEnd) {
            displayBoard();
            System.out.println("Player " + currentPlayer + ", enter your move");
            String input = getUserInput("row and column");
            String[] positions = input.split(",");
            row = Integer.parseInt(positions[0]) - 1;
            col = Integer.parseInt(positions[1]) - 1;

            if (isValidMove(row, col)) {
                board[row][col] = currentPlayer;
                if (isWinningMove(row, col)) {
                    displayBoard();
                    System.out.println("Player " + currentPlayer + " wins!");
                    gameEnd = true;
                } else if (isBoardFull()) {
                    displayBoard();
                    System.out.println("It's a draw!");
                    gameEnd = true;
                } else {
                    currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                }
            } else {
                System.out.println("Invalid move. Please try again.");
            }
        }
    }

    private void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    private void displayBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println("\n-------------");
        }
    }

    private String getUserInput(String coordinate) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Enter " + coordinate + " (row,column): ");
            String input = scanner.nextLine();
            String[] positions = input.split(",");
            if (positions.length == 2) {
                try {
                    int row = Integer.parseInt(positions[0]);
                    int col = Integer.parseInt(positions[1]);
                    if (row >= 1 && row <= 3 && col >= 1 && col <= 3) {
                        return input;
                    }
                } catch (NumberFormatException e) {
                    // Ignore and continue to next iteration
                }
            }
            System.out.println("Invalid input. Please enter two numbers separated by comma (row,column).");
        }
    }

    private boolean isValidMove(int row, int col) {
        if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
            return true;
        }
        return false;
    }

    private boolean isWinningMove(int row, int col) {
        // Check row
        if (board[row][0] == board[row][1] && board[row][1] == board[row][2]) {
            return true;
        }

        // Check column
        if (board[0][col] == board[1][col] && board[1][col] == board[2][col]) {
            return true;
        }

        // Check diagonal
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && (row == col || row + col == 2)) {
            return true;
        }

        return false;
    }

    private boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        OXGame game = new OXGame();
        game.playGame();
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Do you want to play again? (Y/N): ");
        String playAgain = scanner.nextLine();
        if (playAgain.equalsIgnoreCase("Y")) {
            main(args); // เริ่มรอบใหม่
        } else {
            System.out.println("Thank you for playing!");
        }
    }
}



